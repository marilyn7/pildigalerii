﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PictureGallery.Data;
using PictureGallery.Models;
using PictureGallery.ViewModels;

namespace PictureGallery.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            FirstPageContent firstPageContent = new FirstPageContent();
            // 3 newest pictures
            firstPageContent.NewestPictures = _context.Picture.OrderByDescending(x => x.DateCreated).Take(3).ToList();
            // 3 + 3 most liked pictures
            //firstPageContent.MostLikedPictures1stRow = _context.Picture.OrderByDescending(x => x.AmountOfLikes).Take(3).ToList();
            firstPageContent.MostLikedPictures1stRow = _context.Picture.Include(x => x.Album).ThenInclude(x => x.User).Include(x => x.Likes).Where(x => x.Likes != null).OrderByDescending(x => x.Likes.Count()).Take(3).ToList();
            firstPageContent.MostLikedPictures2ndRow = _context.Picture.Include(x => x.Album).ThenInclude(x => x.User).Include(x => x.Likes).Where(x => x.Likes != null).OrderByDescending(x => x.Likes.Count()).Skip(3).Take(3).ToList();

            // 3 newest albums
            firstPageContent.NewestAlbums = _context.Album.OrderByDescending(x => x.DateCreated).Take(3).ToList();
            
            // 3 album covers for the newest albums      

            //Võta kolm kõige uuemat albumit, milles on vähemalt üks pilt ning sorteeri need kuupäeva järgi
            var newestAlbums = _context.Album.Include(x => x.User).Include(x => x.Pictures)
                .Where(x => x.Pictures != null && x.Pictures.Count > 0)
                .OrderByDescending(x => x.DateCreated).Take(3).ToList();

            //Siin teen listi uute albumite kaanepiltidest. Kaanepilt on albumi vanim pilt
            var oldestPictures = new List<Picture>();
            foreach (var album in newestAlbums)
            {
                oldestPictures.Add(album.Pictures.OrderBy(x => x.DateCreated).FirstOrDefault());
            }
            firstPageContent.NewestAlbumCoverPictures = oldestPictures;

            // 3 most faved albums
            var mostFavedAlbums = _context.Album.Include(x => x.Pictures).Where(x => x.Pictures != null && x.Pictures.Count > 0).Include(x => x.Favorites).Where(x => x.Favorites != null).OrderByDescending(x => x.Favorites.Count()).Take(3).ToList();
            var oldestPictures2 = new List<Picture>();
            foreach (var album in mostFavedAlbums)
            {
                oldestPictures2.Add(album.Pictures.OrderBy(x => x.DateCreated).FirstOrDefault());
            }
            firstPageContent.BestAlbumCoverPictures = oldestPictures2;

            // firstPageContent.MostLikedAlbums = _context.Album.OrderByDescending(x => x.DateCreated).Take(3).ToList();
            //// 3 album covers for the newest albums      

            //Võta kolm kõige uuemat albumit, milles on vähemalt üks pilt ning sorteeri need kuupäeva järgi
            //var mostLikedAlbums = _context.Album.Include(x => x.User).Include(x => x.Pictures)
            //    .Where(x => x.Pictures != null && x.Pictures.Count > 0)
            //    .Take(3).ToList();

            ////Siin teen listi populaarsete albumite kaanepiltidest
            //var mostLikedPictures = new List<Picture>();
            //foreach (var album in mostLikedAlbums)
            //{
            //    mostLikedPictures.Add(album.Pictures.OrderByDescending(x => x.AmountOfLikes).FirstOrDefault());
            //}
            //firstPageContent.BestAlbumCoverPictures = mostLikedPictures;

            return View(firstPageContent);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Picture gallery for adding your own content and rating others'.";
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
