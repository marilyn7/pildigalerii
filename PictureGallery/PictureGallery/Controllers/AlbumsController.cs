﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PictureGallery.Data;

namespace PictureGallery.Models
{
    public class AlbumsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AlbumsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Albums
        public async Task<IActionResult> Index()
        {
            // Muudetud, järgmine rida pooleli. Tekita Useritest list, data value field ja text field
            var albums = _context.Album
                .Include(x => x.Favorites)
                .Include(p => p.User);
            // Küsib praeguse kasutaja
            ClaimsPrincipal currentUser = this.User;
            // Kui praegu ei ole ükski kasutaja sisse loginud
            if (currentUser.Identity.Name == null)
            {
                return View(await albums.ToListAsync());
            }
            // Salvestab UserId väärtuse
            var currentUserId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            ViewBag.CurrentUserId = currentUserId;
            foreach (var album in albums)
            {
                var currentFavorite = album.Favorites.Where(x => x.UserId == currentUserId).FirstOrDefault();
                if (currentFavorite != null)
                {
                    album.IsFavorite = currentFavorite.IsFavorite;
                }
            }
            return View(await albums.ToListAsync());
        }

        // uus Index action
        public IActionResult MyIndex()
        {
            // Küsib praeguse kasutaja
            ClaimsPrincipal currentUser = this.User;
            // Kui praegu ei ole ükski kasutaja sisse loginud
            if (currentUser.Identity.Name == null)
            {
                return View(_context.Album.ToList());
            }
            // Salvestab UserId väärtuse
            var currentUserId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            List<Album> Albums = new List<Album>();
            // Võta ainult need albumid, mis on praeguse kasutaja omad
            Albums = _context.Album.Where(x => x.UserId == currentUserId).ToList();
            return View(Albums);
        }

        // GET: Albums/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var album = await _context.Album
                .Include(x => x.User)
                .Include(x => x.Pictures)
                .Include(x => x.Favorites)
                .FirstOrDefaultAsync(m => m.Id == id);
            album.Pictures = album.Pictures.OrderByDescending(x => x.DateCreated).ToList();
            if (album == null)
            {
                return NotFound();
            }

            ClaimsPrincipal currentUser = this.User;
            if (currentUser.Identity.Name == null)
            {
                return View(album);
            }
            var currentUserId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            ViewBag.CurrentUserId = currentUserId;
            if (album.Favorites != null)
            {
                var currentFavorite = album.Favorites.Where(x => x.UserId == currentUserId).FirstOrDefault();
                if (currentFavorite != null)
                {
                    album.IsFavorite = currentFavorite.IsFavorite;
                }
            }


            return View(album);
        }

        // GET: Albums/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Albums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,UserId,DateCreated")] Album album)
        {
            if (ModelState.IsValid)
            {
                // Küsib praeguse kasutaja
                ClaimsPrincipal currentUser = this.User;
                // Salvestab UserId väärtuse
                album.UserId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;

                _context.Add(album);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(MyIndex));
            }
            return View(album);
        }

        // GET: Albums/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var album = await _context.Album.FindAsync(id);
            if (album == null)
            {
                return NotFound();
            }
            return View(album);
        }

        // POST: Albums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,UserId,DateCreated")] Album album)
        {
            if (id != album.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    // Muudetakse ainult albumi title
                    Album config = _context.Album.Find(album.Id);
                    config.Title = album.Title;
                    await _context.SaveChangesAsync();
                    //_context.Update(album);
                    //await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AlbumExists(album.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(MyIndex));
            }
            return View(album);
        }

        // GET: Albums/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var album = await _context.Album
                .FirstOrDefaultAsync(m => m.Id == id);
            if (album == null)
            {
                return NotFound();
            }

            return View(album);
        }

        // POST: Albums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var album = await _context.Album.FindAsync(id);
            _context.Album.Remove(album);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(MyIndex));
        }

        private bool AlbumExists(int id)
        {
            return _context.Album.Any(e => e.Id == id);
        }
    }
}
