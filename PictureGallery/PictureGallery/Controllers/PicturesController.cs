﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PictureGallery.Data;
using PictureGallery.Models;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace PictureGallery.Controllers
{
    public class PicturesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PicturesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Pictures
        public async Task<IActionResult> Index()
        {
            // Näita Albumi Id asemel Albumi Title
            var applicationDbContext = _context.Picture
                .Include(p => p.Album).Include(x => x.Likes).OrderByDescending(x => x.DateCreated);
            var pictures = await applicationDbContext.ToListAsync();

            // Küsib praeguse kasutaja
            ClaimsPrincipal currentUser = this.User;
            // Kui praegu ei ole ükski kasutaja sisse loginud
            if (currentUser.Identity.Name == null)
            {
                return View(_context.Picture.ToList());
            }
            // Salvestab UserId väärtuse
            var currentUserId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            ViewBag.CurrentUserId = currentUserId;

            foreach (var picture in pictures)
            {
                picture.AmountOfLikes =  picture.Likes.Where(x => x.IsLiked).Count();
                var currentLike = picture.Likes.Where(x => x.UserId == currentUserId).FirstOrDefault();
                if (currentLike != null)
                {
                    picture.IsLiked = currentLike.IsLiked;
                }
            }
            return View(pictures);
        }


        // uus Index action
        public IActionResult MyIndex()
        {
            // Küsib praeguse kasutaja
            ClaimsPrincipal currentUser = this.User;
            if (currentUser.Identity.Name == null)
            {
                return View(_context.Picture.ToList());
            }
            // Salvestab UserId väärtuse
            var currentUserId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;

            ViewBag.CurrentUserId = currentUserId;
            List<Picture> Pictures = new List<Picture>();
            // Võta ainult need albumid, mis on praeguse kasutaja omad ja lisa albumid kaasa
            Pictures = _context.Picture
                .Where(x => x.Album.UserId == currentUserId)
                .Include(x => x.Album).OrderByDescending(x => x.DateCreated)
                .ToList();
            
            return View(Pictures);
        }
        // GET: Pictures/Details/5
        public IActionResult Details(int? id, int? albumId)
        {
            if (id == null)
            {
                return NotFound();
            }
            var pictures = _context.Picture
                .Include(x => x.Comments).ThenInclude(x => x.User)
                .Include(x => x.Album).ThenInclude(x => x.User)
                .Include(x => x.Likes)
                .OrderByDescending(x => x.DateCreated).ToList();
            if(albumId != null)
            {
                pictures = pictures.Where(x => x.Album.Id == albumId).ToList();
            }

            var picture = pictures.FirstOrDefault(m => m.Id == id);
            if (picture == null)
            {
                return NotFound();
            }
            picture.Comments = picture.Comments.OrderByDescending(x => x.DateCreated).ToList();
            //List<Picture> Pictures = new List<Picture>();
            //Pictures = _context.Picture.Where(x => x.Album.UserId == currentUserId).Include(x => x.Album).OrderByDescending(x => x.DateCreated).ToList();

            var currentPictureIndex = pictures.IndexOf(picture);
            if (currentPictureIndex > 0)
            {
                ViewBag.PreviousPictureId = pictures[pictures.IndexOf(picture) - 1].Id;
            }

            if (currentPictureIndex != pictures.Count-1)
            {
                ViewBag.NextPictureId = pictures[pictures.IndexOf(picture) + 1].Id;
            }
            if (albumId != null)
            {
                ViewBag.AlbumId = albumId;
            }

            ClaimsPrincipal currentUser = this.User;
            if (currentUser.Identity.Name == null)
            {
                return View(picture);
            }
            var currentUserId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            ViewBag.CurrentUserId = currentUserId;
            if (picture.Likes != null)
            {
                var currentLike = picture.Likes.Where(x => x.UserId == currentUserId).FirstOrDefault();
                if (currentLike != null)
                {
                    picture.IsLiked = currentLike.IsLiked;
                }
            }
            return View(picture);
        }

        // Tagastab suure pildi
        public IActionResult GetImageFile(int pictureId)
        {
            //Kui picture on tühi, siis tagastab stringi "Pilti pole"
            var picture = _context.Picture.Find(pictureId);
            if (picture == null || picture.Image == null)
            {
                return Content("Picture not found.");
            }
            return File(picture.Image, "image/jpg");
        }
        // Tagastab thumbi või kui seda ei ole, siis suure pildi
        public IActionResult GetThumbnail(int pictureId)
        {
            var picture = _context.Picture.Find(pictureId);
            if (picture == null)
            {
                return Content("Picture not found.");
            }
            else if (picture.ImageThumbnail == null)
            {
                return GetImageFile(pictureId);
            }
            return File(picture.ImageThumbnail, "image/jpg");
        }

        // POST: Pictures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveComment(int pictureId, string commentText)
        {
            ClaimsPrincipal currentUser = this.User;
            // Salvestab UserId väärtuse
            var comment = new Comment();
            if (commentText == null)
            {
                // Kui kommentaari väli on tühi, siis seda ei lisata
                ModelState.AddModelError("", "Please type a comment.");
                return RedirectToAction(nameof(Details), new { Id = pictureId });

            }
            comment.Content = commentText;
            comment.PictureId = pictureId;
            if (currentUser.Identity.Name != null)
            {
                comment.UserId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;
            }
            _context.Add(comment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Details), new { Id = pictureId });

        }

        // GET: Pictures/Create
        public IActionResult Create(int albumId)
        {
            // Pilt läheb sellesse albumisse, mille alt ta lisatakse
            ViewData["AlbumId"] = albumId;
            return View();
        }

        // POST: Pictures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,Image,ImageThumbnail,AlbumId,DateCreated")] Picture picture, IFormFile image)
        {
            var albumId = picture.AlbumId;
            ViewData["AlbumId"] = albumId;
            if (image == null)
            {
                ModelState.AddModelError("", "Image file required");
                return View(picture);
            }
            if (ModelState.IsValid)
            {

                MemoryStream stream = new MemoryStream();
                image.CopyTo(stream);

                IImageFormat format;
                // Max mõõtudest suurema pildi vähendamine
                using (Image<Rgba32> imageToResize = Image.Load(stream.ToArray(), out format))
                {
                    stream = new MemoryStream();
                    if (imageToResize.Width > 800)
                    {
                        imageToResize.Mutate(x => x.Resize(800, 0));
                    }
                    if (imageToResize.Height > 600)
                    {
                        imageToResize.Mutate(x => x.Resize(0, 600));
                    }
                    imageToResize.Save(stream, format);
                }
                picture.Image = stream.ToArray();

                //Thumbi genereerimine. Peab olema vähemalt 350 x 250 px ja piirav mõõt peab olema täpne
                using (Image<Rgba32> imageToResize = Image.Load(stream.ToArray(), out format))
                {
                    stream = new MemoryStream();
                    if (imageToResize.Width/(double)imageToResize.Height > 1.4 )
                    {
                        imageToResize.Mutate(x => x.Resize(0, 250));
                    }
                    else
                    {
                        imageToResize.Mutate(x => x.Resize(350, 0));
                    }
                    imageToResize.Save(stream, format);
                }
                picture.ImageThumbnail = stream.ToArray();
                stream.Close();

                _context.Add(picture);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(MyIndex));
            }
            return View(picture);
        }

        // GET: Pictures/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var picture = await _context.Picture.FindAsync(id);
            if (picture == null)
            {
                return NotFound();
            }
            ClaimsPrincipal currentUser = this.User;
            var currentUserId = currentUser.FindFirst(ClaimTypes.NameIdentifier).Value;

            ViewData["AlbumId"] = new SelectList(_context.Album.Where(x => x.UserId == currentUserId), "Id", "Title");
            ViewData["Image"] = picture.Image;
            return View(picture);
        }

        // POST: Pictures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,AlbumId,DateCreated,IsLiked")] Picture picture, IFormFile Image)
        {
            if (id != picture.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    Picture pictureToChange = _context.Picture.Find(picture.Id);
                    pictureToChange.Title = picture.Title;
                    pictureToChange.Description = picture.Description;
                    pictureToChange.AlbumId = picture.AlbumId;

                    await _context.SaveChangesAsync();

                    //MemoryStream stream = new MemoryStream();
                    //    Image.CopyTo(stream);
                    //    picture.Image = stream.ToArray();
                    //    stream.Close();

                    //_context.Update(picture);
                    //await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PictureExists(picture.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(MyIndex));
            }
            return View(picture);
        }
        [HttpPost]
        public async Task<IActionResult> ChangeIsLiked(int id)
        {
            var picture = await _context.Picture.FindAsync(id);
            picture.IsLiked = picture.IsLiked == true ? false : true;
            _context.Picture.Update(picture);
            await _context.SaveChangesAsync();
            return Json(picture);
        }

        //[HttpPost]
        //public async Task<IActionResult> LikeCheck(Like like)
        //{
        //    _context.Like.Add(like);
        //    like.IsLiked = like.IsLiked == true ? false : true;
        //    _context.Like.Update(like);
        //    await _context.SaveChangesAsync();
        //    return Json(like.IsLiked);
        //}

        // GET: Pictures/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var picture = await _context.Picture
                .FirstOrDefaultAsync(m => m.Id == id);
            if (picture == null)
            {
                return NotFound();
            }
            var applicationDbContext = _context.Picture.Include(p => p.Album);
            await applicationDbContext.ToListAsync();
            return View(picture);
        }

        // POST: Pictures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var picture = await _context.Picture.FindAsync(id);
            _context.Picture.Remove(picture);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(MyIndex));
        }

        private bool PictureExists(int id)
        {
            return _context.Picture.Any(e => e.Id == id);
        }
    }
}
