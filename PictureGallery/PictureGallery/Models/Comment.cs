﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PictureGallery.Models
{
    public class Comment
    {

        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int PictureId { get; set; }
        [Required]
        public string Content { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }
        public Picture Picture { get; set; }

        public Comment()
        {
            DateCreated = DateTime.Now;
        }
    }
}
