﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PictureGallery.Models
{
    public class Favorite
    {
        public int Id { get; set; }
        public int AlbumId { get; set; }
        public Album Album { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }      
        public bool IsFavorite { get; set; }
    }
}
