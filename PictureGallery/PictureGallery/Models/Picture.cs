﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PictureGallery.Models
{
    public class Picture
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        //[Required]
        public byte[] Image { get; set; }
        public byte[] ImageThumbnail { get; set; }
        public int AlbumId { get; set; }
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; private set; }
        public Album Album { get; set; }
        public List<Comment> Comments { get; set; }
        [Display(Name = "Likes")]
        public int AmountOfLikes { get; set; }

        [NotMapped]
        public bool IsLiked { get; set; }

        public List<Like> Likes { get; set; }




        public Picture()
        {
            DateCreated = DateTime.Now;
        }

    }
}
