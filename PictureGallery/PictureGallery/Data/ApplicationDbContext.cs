﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PictureGallery.Models;

namespace PictureGallery.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<PictureGallery.Models.Album> Album { get; set; }
        public DbSet<PictureGallery.Models.Comment> Comment { get; set; }
        public DbSet<PictureGallery.Models.Picture> Picture { get; set; }
        public DbSet<PictureGallery.Models.Like> Like { get; set; }
        public DbSet<PictureGallery.Models.Favorite> Favorite { get; set; }


    }
}
