﻿using PictureGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PictureGallery.ViewModels
{
    public class FirstPageContent
    {
        public List<Picture> NewestPictures { get; set; }
        public List<Picture> MostLikedPictures1stRow { get; set; }
        public List<Picture> MostLikedPictures2ndRow { get; set; }
        public List<Album> NewestAlbums { get; set; }
        public List<Album> MostLikedAlbums { get; set; }
        public List<Picture> NewestAlbumCoverPictures { get; set; }
        public List<Picture> BestAlbumCoverPictures { get; set; }


    }
}
